( function( $, window ) {

    "use strict";

    /**
     * Class Infiscroll
     *
     * @constructor
     *
     * @param {Object} container
     * @param {Object} options
     */
    function Infiscroll( container, options ) {

        /**
         * The wrapped plugin attached container
         *
         * @type {Object}
         */
        this._$container = $( container );

        /**
         * Plugin settings
         *
         * @type {Object}
         */
        this._settings = $.extend(

            // empty as jQuery mutates the first object
            {},

            // default settings
            {
                container_selector: ".posts",
                item_selector: ".post",
                pagination_selector: ".pagination",
                next_selector: ".next",
                show_button: false,
                show_load_all_button: false,
                button_text: "Load More",
                show_loader: true,
                load_all_button_text: "Load All",
                loader: function( text ) {
                    return "<p>" + text + "</p>";
                },
                loader_text: "Loading...",
                loader_img: "",
                scroll_container: window,
                offset: 0,
                // a function to filter the retreived posts for each page
                filter_posts: false,
            },

            // given instance options
            options
        );

        /**
         * The pagination element
         *
         * @type {Object}
         */
        this._$pagination = $( this._settings.pagination_selector );

        // hide the pagination
        this._$pagination.hide();

        /**
         * The link to the next page
         *
         * @type {String|undefined}
         */
        this._next_link = this._$pagination.find( this._settings.next_selector ).attr( "href" );

        /**
         * The number of pages that have been loaded
         *
         * @type {Number}
         */
        this._num_pages = 0;

        if ( this.has_next() ) {
            if ( this._settings.show_button ) {
                /**
                 * The load more button element
                 *
                 * @type {Object}
                 */
                this._button_element = $( "<button type=\"button\" class=\"infiscroll-button\">" + this._settings.button_text + "</button>" );

                /**
                 * The load all button element
                 *
                 * @type {Object}
                 */
                this._load_all_button_element = $( "<button type=\"button\" class=\"infiscroll-button\">" + this._settings.load_all_button_text + "</button>" );

                // bind button click event
                this._button_element.on( "click", this.get_next_page.bind( this ) );
                this._load_all_button_element.on( "click", this.get_all_pages.bind( this ) );

                this.show_button();
            } else {
                /**
                 * The scroll container
                 *
                 * @type {Objcet}
                 */
                this._$scroll_container = $( this._settings.scroll_container );

                this._scroll_container_inner_height = this._$scroll_container.innerHeight();
                this._$last_post = this._$container.find( this._settings.item_selector ).last();
                this._container_bottom = this._$last_post.offset().top + this._$last_post.outerHeight();

                // bind to the scroll event
                this._$scroll_container.on( "scroll.infiscroll", this._scroll_listener.bind( this ) );
            }

            if ( this._settings.show_loader ) {
                /**
                 * The loader element
                 *
                 * @type {Object}
                 */
                this._$loader_element = $( "<div class=\"infiscroll-loader\">" ).html( this._settings.loader( this._settings.loader_text, this._settings.loader_img ) );
            }
        }
    }

    /**
     * Returns the plugin settings
     *
     * @return {Object}
     */
    Infiscroll.prototype.get_settings = function() {

        return this._settings;
    };

    /**
     * Returns true if there is a next page
     *
     * @return {Boolean}
     */
    Infiscroll.prototype.has_next = function() {

        return this._next_link !== void 0;
    };

    /**
     * Displays the load more button
     *
     * @return {undefined}
     */
    Infiscroll.prototype.show_button = function() {

        this._$pagination.html( this._button_element );

        if ( this._settings.show_load_all_button ) {
            this._button_element.after( this._load_all_button_element );
        }

        this._$pagination.show();
    };

    /**
     * Hides the load more button
     *
     * @return {undefined}
     */
    Infiscroll.prototype.hide_button = function() {

        this._$pagination.hide();
    };

    /**
     * Listens to the scroll event
     *
     * @param  {Object} event
     *
     * @return {undefined}
     */
    Infiscroll.prototype._scroll_listener = function( event ) {

        var scroll_bottom = this._$scroll_container.scrollTop() +
            this._scroll_container_inner_height -
            this._settings.offset;

        if ( scroll_bottom >= this._container_bottom ) {
            this._$scroll_container.off( "scroll.infiscroll" );
            this.get_next_page();
        }
    };

    /**
     * Retrieves the next set of posts
     *
     * @param {Function} callback
     *
     * @return {undefined}
     */
    Infiscroll.prototype.get_next_page = function( callback ) {

        if ( this.has_next() ) {

            // emit event
            this._$container.trigger( "get_next_page:start" );

            // display loader
            if ( this._settings.show_loader ) {
                this._$container.after( this._$loader_element );
            }

            $.ajax({
                url: this._next_link,
                method: "GET",
                dataType: "html",
                success: function( data ) {

                    // increment the number of pages that have been loaded
                    ++this._num_pages;

                    var $posts = $( data, this._settings.container_selector ).find( this._settings.item_selector );
                    var $pagination = $( data ).find( this._settings.pagination_selector );

                    // filter posts
                    if ( typeof this._settings.filter_posts === "function" ) {
                        $posts = this._settings.filter_posts( $posts );
                    }

                    // append new posts
                    this._$container.find( this._settings.item_selector ).last().after( $posts );

                    // emit event
                    this._$container.trigger( "get_next_page:done", { posts: $posts } );

                    // remove loader
                    if ( this._settings.show_loader ) {
                        this._$loader_element.remove();
                    }

                    // update next link
                    this._next_link = $pagination.find( this._settings.next_selector ).attr( "href" );

                    // update status
                    if ( this.has_next() ) {
                        if ( ! this._settings.show_button ) {
                            this._scroll_container_inner_height = this._$scroll_container.innerHeight();
                            this._$last_post = this._$container.find( this._settings.item_selector ).last();
                            this._container_bottom = this._$last_post.offset().top + this._$last_post.outerHeight();

                            // rebind scroll listener
                            this._$scroll_container.on( "scroll.infiscroll", this._scroll_listener.bind( this ) );
                        }
                    } else {
                        if ( this._settings.show_button ) {
                            this.hide_button();
                        }
                    }

                    if ( typeof callback === "function" ) {
                        callback.call( callback, $posts );
                    }
                }.bind( this ),
                error: function( err ) {
                    // @todo: hide loader

                    // trigger event
                    this._$container.trigger( "get_next_page:fail" );
                    this._$container.trigger( "get_next_page:done", { posts: [] } );

                    if ( typeof callback === "function" ) {
                        callback.call( callback, err );
                    }
                }
            });
        }
    };

    /**
     * Recursively retrieves all posts
     *
     * @return {undefined}
     */
    Infiscroll.prototype.get_all_pages = function() {

        if ( this.has_next() ) {
            this.get_next_page( function() {
                this.get_all_pages();
            }.bind( this ) );
        }
    };

    // bootstrap the plugin
    $.fn.infiscroll = function( options ) {

        if ( options === void 0 ) {
            options = {};
        }

        if ( typeof options !== "object" ) {
            throw new TypeError( "Usage: infiscroll(Object options)" );
        }

        for ( var i = 0, l = this.length; i < l; i++ ) {

            var container = this[ i ];

            if ( $.data( container, "infiscroll" ) === void 0 ) {

                $.data( container, "infiscroll", new Infiscroll( container, options ) );
            }
        }

        return this;
    };
} )( jQuery, window );
